﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Server
{
    class ClientHandler
    {
        #region Fields
        Socket clientSocket = null;
        string name = null;
        StringBuilder data;
        byte[] buffer = new byte[256];
        int size = 0;
        bool isMe;
        public StringBuilder Data { get => data; set => data = value; }

        public Socket ClientSocket { get => clientSocket; set => clientSocket = value; }
        public byte[] Buffer { get => buffer; set => buffer = value; }
        public int Size { get => size; set => size = value; }
        public bool IsMe { get => isMe; set => isMe = value; }

        Thread thread;

        #endregion

        public ClientHandler(Socket socket)
        {
            this.ClientSocket = socket;
            Data = new StringBuilder();
            this.thread = new Thread(ClientStart);
            thread.Start();
        }

        void ClientStart()
        {
            try
            {
                while (true)
                {

                    HandleClient();

                }
            }
            catch (SocketException)
            {
                Console.WriteLine("One of clients has left the chat");
            }
        }
        void DataClear()
        {
            Size = 0;
            Data.Clear();
            Buffer = new Byte[256];
            MainClass.ServerBuffer = new Byte[256];
            IsMe = false;
        }
        void HandleClient()
        {
            do
            {
                Size = clientSocket.Receive(Buffer);
                Data.Append(Encoding.UTF8.GetString(Buffer, 0, Size));

            } while (clientSocket.Available > 0);
            Buffer = Encoding.UTF8.GetBytes(Data.ToString());
            IsMe = true;
            if(Buffer != null)
                MainClass.SendToAllTheClients(Buffer);
            DataClear();
        }
    }
}
