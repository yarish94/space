﻿

using Newtonsoft.Json;

namespace Server.JsonSupport
{
    public static class JsonSettings
    {
        public static JsonSerializerSettings GetJsonSettings()
        {
            var settings = new JsonSerializerSettings();
            settings.Converters.Add(new IPAddressConverter());
            //settings.Converters.Add(new IPEndPointConverter());
            settings.Formatting = Formatting.Indented;
            return settings;
        }
    }
}
