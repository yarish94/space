﻿using Newtonsoft.Json;
using Server.JsonSupport;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Server
{
    static class MainClass
    {
        private static Socket tcpSocket = null;
        private static ServerSettings settings = ServerSettings.GetSettings();
        private static List<ClientHandler> clients = new List<ClientHandler>();
        public static byte[] ServerBuffer { get; set; }
        static void Main(string[] args)
        {
            tcpSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            TryBind(settings.tcpEndPoint);
            tcpSocket.Listen(5);
            Console.WriteLine("Listening:");

            while (true)
            {
                var listener = tcpSocket.Accept();
                Console.WriteLine("Catch one");
                ClientHandler client = new ClientHandler(listener);
                clients.Add(client);
            }
        }
        public static void SendToAllTheClients(byte[] buffer)
        {
            //if (clients.Count < 2) return;
            foreach (var el in clients)
            {
                if (el.IsMe == false)
                {
                    el.ClientSocket.Send(buffer);
                }
            }
        }
        private static void TryBind(IPEndPoint iPEndPoint)
        {
            try
            {
                tcpSocket.Bind(iPEndPoint);
            }
            catch (SocketException)
            {
                settings = new ServerSettings();
                tcpSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                tcpSocket.Bind(settings.tcpEndPoint);
            }
        }
    }
}
