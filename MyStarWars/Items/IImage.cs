﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyStarWars.Items
{
    interface IImage
    {

        int X { get; set; }
        int Y { get; set; }
        int Image { get; set; }
        int[] Size { get; }

    }
}
