﻿using MyStarWars.UI;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
namespace MyStarWars.Items
{

    public class Spaceship : IImage, IItem
    {
        [JsonProperty("X")]
        public int X { get; set; }
        [JsonProperty("Y")]
        public int Y { get; set; }
        [JsonProperty("IsAlive")]
        public bool IsAlive { get; set; }
        [JsonProperty("Image")]
        public int Image { get; set; }
        public int Height = 500;
        public int Width = 750;
        public static int[] size = new int[2] {60, 60};
        public readonly Dictionary<string, int> borders;
        public int[] Size
        {
            get
            {
                return size;
            }
        }
        private int dS = 30;

        public Spaceship()
        {
        }

        public Spaceship(int x, int y, Dictionary<string, int> _borders)
        {
            X = x;
            Y = y;
            IsAlive = true;
            this.borders = _borders;
        }

        public void moveDown()
        {
            Y += dS;
            if ((Y + size[1]) > Height) Y = Height - dS - size[1];
        }

        public void moveLeft()
        {
            X -= dS;
            if (X < 0) X += dS;
        }

        public void moveRight()
        {
            X += dS;
            if ((X + size[1]) > Width) X = Width - dS - size[1];
        }

        public void moveUp()
        {
            Y -= dS;
            if (Y < 0) Y = +dS;
        }

        public void Move(Keys keyCode)
        {
            switch (keyCode)
            {
                case Keys.Up:
                    moveUp();
                    break;
                case Keys.Down:
                    moveDown();
                    break;
                case Keys.Left:
                    moveLeft();
                    break;
                case Keys.Right:
                    moveRight();
                    break;
            }
        }
    }
}
