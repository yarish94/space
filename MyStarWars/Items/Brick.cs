﻿using System;
using System.Windows.Forms;

namespace MyStarWars.Items
{
    public class Brick
    {
        public int X { get; set; }
        public int Y { get; set; }
    }
}
