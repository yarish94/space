﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyStarWars.Items
{
    public interface IItem
    {

        void Move(Keys keyCode);
        bool IsAlive { get; set; }
        int X { get; set; }
        int Y { get; set; }
        int Image { get; set; }

    }
}
