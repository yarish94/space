﻿using MyStarWars.Client;
using MyStarWars.Items;
using MyStarWars.UI;
using Newtonsoft.Json;
using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MyStarWars
{
    class Controller
    {

        private readonly GUI view;
        private readonly Model model;
        private readonly MyClient client;



        public Controller(GUI _view, Model model)
        {
            view = _view;
            this.model = model;
            client = new MyClient();
            view.SendData += View_SendData;
            client.ConnectionProblems += Client_ConnectionProblems;
            client.Update += Client_Update;
        }


        private async void View_SendData(object sender, string e)
        {
            try
            {
                await Task.Run(() => client.SendData(e));
            }
            catch(Exception ex)
            {
                MessageService.ShowError(ex.Message);
            }
        }

        private void Client_Update(object sender, string e)
        {
            Spaceship temp = JsonConvert.DeserializeObject<Spaceship>(e.ToString());
            view.Ships[1].X = temp.X;
            view.Ships[1].Y = temp.Y;
            view.Ships[1].IsAlive = temp.IsAlive;
            view.Ships[1].Image = temp.Image;
            view.PrintElements();
  }

        private void Client_ConnectionProblems(object sender, string e)
        {
            MessageService.ShowError(e);
        }

        private void View_GameOver(object sender, int e)
        {
            model.SaveScore(e);
            MessageService.ShowMessage("You lost");
        }
    }
}
