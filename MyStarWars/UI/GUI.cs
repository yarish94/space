﻿using MyStarWars.Items;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyStarWars.UI
{
    public interface IGameFieldForm
    {
        IItem[] Ships { get; set; }
    }


    public partial class GUI : Form
    {

        public event EventHandler<string> SendData;
        public delegate void Invoker();

        private readonly int height;
        private readonly int width;

        //public static int Width
        //{
        //    get
        //    {
        //        return gameField.Size.Height;
        //    }
        //}
        public static int Height { get; }

        public IItem[] Ships { get; set; }

        Image[] shipImages = {Properties.Resources.Spaceship,
                              Properties.Resources.Camaz,
                              Properties.Resources.Helicopter,
                              Properties.Resources.Plane,
                              Properties.Resources.Taxi,
                              Properties.Resources.Titanic};
        public GUI()
        {
            InitializeComponent();
            this.PreviewKeyDown += GUI_PreviewKeyDown;
            this.KeyDown += GUI_KeyDown;

            height = gameField.Size.Height;
            width = gameField.Size.Width;
          
            Ships = new IItem[2];
            Ships[0] = new Spaceship(0, gameField.Size.Height - Spaceship.size[1], GetBorders());
            Ships[1] = new Spaceship(0, gameField.Size.Height - Spaceship.size[1], GetBorders());
        }

        private void GUI_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Up:
                case Keys.Down:
                case Keys.Left:
                case Keys.Right:
                    e.IsInputKey = true;
                break;
            }
        }
        private void GUI_KeyDown(object sender, KeyEventArgs e)
        {
                switch (e.KeyCode)
                {
                    case Keys.Up:
                    case Keys.Down:
                    case Keys.Left:
                    case Keys.Right:
                    Ships[0].Move(e.KeyCode);
                        break;
                    case Keys.Space:
                        return;
                    case Keys.A:
                        MessageService.ShowMessage("Это пиздец, работает:))");
                        return;
                }
            SendData?.Invoke(this, JsonConvert.SerializeObject(Ships[0]));
            PrintElements();      
        }
        private void btnChoose_Click(object sender, EventArgs e)
        {
            Form chooseForm = new FormChoose(ref Ships[0]);
            chooseForm.Closing += ChooseForm_Closing;
            chooseForm.Show();;
        }
        private void ChooseForm_Closing(object sender, CancelEventArgs e)
        {
            SendData?.Invoke(this, JsonConvert.SerializeObject(Ships[0]));
            gameField.Controls.Add(GetShipPicture((IImage)Ships[0]));
            btnChoose.Enabled = false;
            btnStart.Enabled = false;
            btnStop.Enabled = false;
            Refresh();
        }
        private PictureBox GetShipPicture(IImage image)
        {

            PictureBox picture = new PictureBox()
            {
                BorderStyle = BorderStyle.None,
                Image = shipImages[image.Image - 1],
                Location = new System.Drawing.Point(image.X, image.Y),
                Name = "pictureBox",
                Size = new System.Drawing.Size(image.Size[0], image.Size[1]),
                SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage,
                TabIndex = 0,
                TabStop = false
            };
            return picture ;
        }
        public void PrintElements()
        {
            this.BeginInvoke(new Invoker(UpdateControl));
        }
        public void UpdateControl()
        {
            gameField.Controls.Clear();
            foreach (var el in Ships)
            {
                if (el.Image != 0)
                {
                    gameField.Controls.Add(GetShipPicture((IImage)el));
                    Refresh();
                }

            }
        }
        private Dictionary<string, int> GetBorders()
        {
            Dictionary<string, int> map = new Dictionary<string, int>();
            map.Add("minX", 0);
            map.Add("minY", 0);
            map.Add("maxX", width);
            map.Add("maxY", height);
            return map;
        }
    }
}
