﻿using MyStarWars.Client;
using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MyStarWars
{
    class MyClient
    {
        Socket tcpSocket;
        private static ClientSettings settings = ClientSettings.GetSettings();
        public event EventHandler<string> ConnectionProblems;
        public event EventHandler<string> Update;
        public MyClient()
        {
            tcpSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            try
            {
                tcpSocket.Connect(settings.tcpEndPoint);
                RecieveAsync();
            }
            catch (SocketException ex)
            {
                throw new Exception("Check ClientSetting");
            }

        }

        async void RecieveAsync()
        {
            await Task.Run(() => Recieve());
        }
        void Recieve()
        {
            while (true)
            {
                byte[] buffer = new byte[256];
                int size = 0;
                StringBuilder data = new StringBuilder();
                do
                {
                    size = tcpSocket.Receive(buffer);
                    data.Append(Encoding.UTF8.GetString(buffer, 0, size));

                } while (tcpSocket.Available > 0);
                if (Update != null && !String.IsNullOrEmpty(data.ToString())) 
                    Update(this, data.ToString());
            }
        }

        public void SendData(string data)
        {
            if(!String.IsNullOrEmpty(data))
            {
                tcpSocket.Send(Encoding.UTF8.GetBytes(data));
            }
            else
            {
                throw new Exception("Попытка отправки пустого сообщения на сервер!");
            }
        }
    }
}
