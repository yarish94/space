﻿
using MyStarWars.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyStarWars
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            GUI form = new GUI();
            Model model = new ModelText();
            Controller controller = new Controller(form, model, new Client.Settings("192.168.1.67", 7000));
            Controller controller = new Controller(form, model);

            Application.Run(form);
            //такое чувство, что стартовать нужно с контроллера
        }
    }
}
