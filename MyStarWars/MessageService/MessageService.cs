﻿using System.Windows.Forms;

namespace MyStarWars
{
    interface IMessageService
    {
        void ShowMessage(string message);
        void ShowError(string message);
    }

    public static class MessageService
    {
        public static void ShowError(string message)
        {
            MessageBox.Show(message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public static void ShowMessage(string message)
        {
            MessageBox.Show(message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}
